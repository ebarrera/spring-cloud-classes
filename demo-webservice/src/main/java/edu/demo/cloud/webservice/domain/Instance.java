package edu.demo.cloud.webservice.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
public class Instance {

    private Long id;
    private String name;
    private String token;
    private Boolean valid;
    private Boolean disabled;
    private String externalAuthentication;

}

package edu.demo.cloud.webservice.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/demo")
@Api(value="demo controller", description="Demo controller documentation")
public class DemoController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DemoController.class);

    @ApiOperation(value = "Test endpoint")
    @GetMapping("/test")
    public Map test(){
        Map<String, String> map = new HashMap<>();
        map.put("status", "OK");
        return map;
    }

}

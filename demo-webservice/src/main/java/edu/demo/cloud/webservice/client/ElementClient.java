package edu.demo.cloud.webservice.client;

import edu.demo.cloud.webservice.domain.Instance;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface ElementClient {

    CompletableFuture<List<Instance>> instances();

}

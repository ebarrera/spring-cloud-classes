package edu.demo.cloud.webservice.client;

import edu.demo.cloud.webservice.config.CloudElementsConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class ClientFactory {

    @Autowired
    private CloudElementsConfig config;

    @Bean
    public ElementClient elementClient(){
        ElementClient client = new ElementClientImpl(config.apiURL(), config.credentials());
        return client;
    }

}

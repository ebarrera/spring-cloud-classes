package edu.demo.cloud.webservice.controller;

import edu.demo.cloud.webservice.client.ElementClient;
import edu.demo.cloud.webservice.domain.Instance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/elements")
public class ElementsController {

    @Autowired
    private ElementClient elementClient;

    @GetMapping(value = "/instances")
    public List<Instance> instances() {

        try {
            return elementClient.instances().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


        return Collections.emptyList();
    }

}

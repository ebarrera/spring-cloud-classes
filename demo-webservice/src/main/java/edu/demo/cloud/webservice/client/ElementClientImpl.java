package edu.demo.cloud.webservice.client;

import edu.demo.cloud.webservice.domain.Instance;
import edu.demo.cloud.webservice.utils.JsonUtils;
import edu.demo.cloud.webservice.utils.StringConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Slf4j
public class ElementClientImpl implements ElementClient {

    private RestTemplate template;
    private String url, credentials;

    public ElementClientImpl(String url, String credentials){
        this.template = new RestTemplate();
        this.url = url;
        this.credentials = credentials;
        initTemplate();
    }

    private void initTemplate(){
        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
        messageConverter.setPrettyPrint(false);
        messageConverter.setObjectMapper(JsonUtils.mapper());
        template.getMessageConverters().add(messageConverter);
    }

    @Override
    public CompletableFuture<List<Instance>> instances() {

        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, credentials);
        headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        HttpEntity<List<Instance>> entity = new HttpEntity<>(headers);

        String instancesURL = url.concat(StringConstants.URL_SEPARATOR).concat("instances");

        ResponseEntity<List<Instance>> response = template.exchange(instancesURL,
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<Instance>>(){});

        List<Instance> expected = response.getBody();

        return CompletableFuture.completedFuture(expected);
    }
}

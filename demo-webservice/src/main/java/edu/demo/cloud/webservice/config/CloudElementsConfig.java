package edu.demo.cloud.webservice.config;

import edu.demo.cloud.webservice.utils.StringConstants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
public class CloudElementsConfig {

    @Value("${cloud.elements.user}")
    private String user;

    @Value("${cloud.elements.org}")
    private String org;

    @Value("${cloud.elements.baseURL}")
    private String baseURL;

    @Value("${cloud.elements.api}")
    private String api;

    public String apiURL(){
        return baseURL.concat(api);
    }

    public String credentials(){
        StringBuilder builder = new StringBuilder();

        return builder.append("User")
                .append(StringConstants.SPACE)
                .append(user)
                .append(StringConstants.COMMA)
                .append(StringConstants.SPACE)
                .append("Organization")
                .append(StringConstants.SPACE)
                .append(org).toString();
    }

}

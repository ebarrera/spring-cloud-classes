package edu.demo.cloud.webservice.utils;

public final class StringConstants {

    private StringConstants(){}

    public static final String COMMA = ",";
    public static final String SPACE = " ";
    public static final String URL_SEPARATOR = "/";

}
